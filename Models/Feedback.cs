using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.ModelBinding;


namespace bakery.Models
{
	public class Feedback
	{
		[BindNever]
		public int Id { get; set; }

		[Required]
		[StringLength(64)]
		public string Name { get; set; }

		[Required]
		[StringLength(64)]
		[DataType(DataType.EmailAddress)]
		public string Email { get; set; }

		[Required]
		[StringLength(512)]
		public string Message { get; set; }

		[Display(Name = "Contact Me")]
		public bool ContactMe { get; set; }
	}
}

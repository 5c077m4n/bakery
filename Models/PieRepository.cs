using System.Collections.Generic;
using System.Linq;


namespace bakery.Models
{
	public class PieRepository : IPieRepository
	{
		private AppDbContext _appDbContext;

		public PieRepository(AppDbContext appDbContext)
		{
			_appDbContext = appDbContext;
		}
		public IEnumerable<Pie> GetAllPies()
		{
			return _appDbContext.Pies;
		}

		public Pie GetPieById(int pieId)
		{
			return _appDbContext.Pies.FirstOrDefault(pie => pie.Id == pieId);
		}
	}
}

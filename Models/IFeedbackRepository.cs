namespace bakery.Models
{
	public interface IFeedbackRepository
	{
		void AddFeedback(Feedback feedback);
	}
}

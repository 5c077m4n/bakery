using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Collections.Generic;

using bakery.Models;
using bakery.ViewModels;


namespace bakery.Controllers
{
	public class HomeController : Controller
	{
		private readonly IPieRepository _pieRepository;
		public HomeController(IPieRepository pieRepository)
		{
			_pieRepository = pieRepository;
		}
		public IActionResult Index()
		{
			HomeViewModel hvm = new HomeViewModel
			{
				Title = "Welcome to Shapira's Pie Shop!",
				Pies = _pieRepository.GetAllPies().OrderBy(pie => pie.Name).ToList()
			};
			return View(hvm);
		}
		public IActionResult Details(int id)
		{
			Pie pie = _pieRepository.GetPieById(id);
			if (pie == null) return NotFound();
			return View(pie);
		}
	}
}

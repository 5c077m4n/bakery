using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.FileProviders;


namespace Microsoft.AspNetCore.Builder
{
	public static class ApplicationBuilderExtension
	{
		public static IApplicationBuilder UseNodeModules(
			this IApplicationBuilder app,
			string rootPath
		)
		{
			string path = Path.Combine(rootPath, "node_modules");
			PhysicalFileProvider pfp = new PhysicalFileProvider(path);

			StaticFileOptions options = new StaticFileOptions();
			options.RequestPath = "/node_modules";
			options.FileProvider = pfp;

			app.UseStaticFiles(options);
			return app;
		}
	}
}
